/* gmpc-serverstats (GMPC plugin)
 * Copyright (C) 2006-2009 Qball Cow <qball@sarine.nl>
 * Project homepage: http://gmpcwiki.sarine.nl/
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef __SERVERSTATS_PLUGIN_H__
#define __SERVERSTATS_PLUGIN_H__
void serverstats_set_enabled(int enabled);
int serverstats_get_enabled(void);
static void serverstats_add(GtkWidget *category_tree);

static void serverstats_selected(GtkWidget *container);

static void serverstats_unselected(GtkWidget *container);

void serverstats_plugin_init(void);
void serverstats_connection_changed(MpdObj *mi, int connect,void *userdata);
#endif
